/**
 * @file april_tags.cpp
 * @brief Example application for April tags library
 * @author: Michael Kaess
 *
 * Opens the first available camera (typically a built in camera in a
 * laptop) and continuously detects April tags in the incoming
 * images. Detections are both visualized in the live image and shown
 * in the text console. Optionally allows selecting of a specific
 * camera in case multiple ones are present and specifying image
 * resolution as long as supported by the camera. Also includes the
 * option to send tag detections via a serial port, for example when
 * running on a Raspberry Pi that is connected to an Arduino.
 * 
 * Added Lucas Kanade Tracking after initial detection and ROS publisher
 * @author: Tushar Agrawal
 *
 */
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

#include <iostream>
#include <iomanip>
#include <cstring>
#include <vector>
#include <list>
#include <sys/time.h>

// ROS
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>

const string usage = "\n"
  "Usage:\n"
  "  apriltags_demo [OPTION...] [IMG1 [IMG2...]]\n"
  "\n"
  "Options:\n"
  "  -h  -?          Show help options\n"
  "  -a              Arduino (send tag ids over serial port)\n"
  "  -r              Send through ROS /apriltag/local\n"
  "  -d              Disable graphics\n"
  "  -t              Timing of tag extraction\n"
  "  -C <bbxhh>      Tag family (default 36h11)\n"
  "  -D <id>         Video device ID (if multiple cameras present)\n"
  "  -F <fx>         Focal length in pixels\n"
  "  -W <width>      Image width (default 640, availability depends on camera)\n"
  "  -H <height>     Image height (default 480, availability depends on camera)\n"
  "  -S <size>       Tag size (square black frame) in meters\n"
  "  -E <exposure>   Manually set camera exposure (default auto; range 0-10000)\n"
  "  -G <gain>       Manually set camera gain (default auto; range 0-255)\n"
  "  -B <brightness> Manually set the camera brightness (default 128; range 0-255)\n"
  "\n";

const string intro = "\n"
    "April tags test code\n"
    "(C) 2012-2014 Massachusetts Institute of Technology\n"
    "Michael Kaess\n"
    "\n";


#ifndef __APPLE__
#define EXPOSURE_CONTROL // only works in Linux
#endif

#ifdef EXPOSURE_CONTROL
#include <libv4l2.h>
#include <linux/videodev2.h>
#include <fcntl.h>
#include <errno.h>
#endif

// OpenCV library for easy access to USB camera and drawing of images
// on screen
#include "opencv2/opencv.hpp"

// April tags detector and various families that can be selected by command line option
#include "AprilTags/TagDetector.h"
#include "AprilTags/Tag16h5.h"
#include "AprilTags/Tag25h7.h"
#include "AprilTags/Tag25h9.h"
#include "AprilTags/Tag36h9.h"
#include "AprilTags/Tag36h11.h"


// Needed for getopt / command line options processing
#include <unistd.h>
extern int optind;
extern char *optarg;

// For Arduino: locally defined serial port access class
#include "Serial.h"


const char* windowName = "apriltags_demo";


// utility function to provide current system time (used below in
// determining frame rate at which images are being processed)
double tic() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((double)t.tv_sec + ((double)t.tv_usec)/1000000.);
}
cv::Point2f point;

#define RESET_FRAME_NO 50
#define BIG_TAG_ID 166
#define SMALL_TAG_ID 138
#define BIG_TAG_SIZE 0.575
#define SMALL_TAG_SIZE BIG_TAG_SIZE/10
#define DEFAULT_CAMERA 1

#include <cmath>

/* Keep the webcam from locking up when you interrupt a frame capture */
volatile int quit_signal=0;
#ifdef __unix__
#include <signal.h>
extern "C" void quit_signal_handler(int signum) {
 if (quit_signal!=0) exit(0); // just exit already
 quit_signal=1;
 printf("Will quit at next camera frame (repeat to kill now)\n");
}
#endif

#ifndef PI
const double PI = 3.14159265358979323846;
#endif
const double TWOPI = 2.0*PI;

/**
 * Normalize angle to be within the interval [-pi,pi].
 */
inline double standardRad(double t) {
  if (t >= 0.) {
    t = fmod(t+PI, TWOPI) - PI;
  } else {
    t = fmod(t-PI, -TWOPI) + PI;
  }
  return t;
}

/**
 * Convert rotation matrix to Euler angles
 */
void wRo_to_euler(const Eigen::Matrix3d& wRo, double& yaw, double& pitch, double& roll) {
    yaw = standardRad(atan2(wRo(1,0), wRo(0,0)));
    double c = cos(yaw);
    double s = sin(yaw);
    pitch = standardRad(atan2(-wRo(2,0), wRo(0,0)*c + wRo(1,0)*s));
    roll  = standardRad(atan2(wRo(0,2)*s - wRo(1,2)*c, -wRo(0,1)*s + wRo(1,1)*c));
}

class Demo {

  AprilTags::TagDetector* m_tagDetector;
  AprilTags::TagCodes m_tagCodes;

  bool m_draw; // draw image and April tag detections?
  bool m_arduino; // send tag detections to serial port?
  bool m_timing; // print timing information for each tag extraction call

  int m_width; // image size in pixels
  int m_height;
  double m_tagSize; // April tag side length in meters of square black frame
  double m_fx; // camera focal length in pixels
  double m_fy;
  double m_px; // camera principal point
  double m_py;

  int m_deviceId; // camera id (in case of multiple cameras)

  int m_ros;
  ros::NodeHandle nh;
  ros::Publisher tag_pose_pub;

  list<string> m_imgNames;

  cv::VideoCapture m_cap;

  int m_exposure;
  int m_gain;
  int m_brightness;

  Serial m_serial;

  std::vector<cv::Point2f> points[2];
  static const int MAX_COUNT = 500;

  vector<AprilTags::TagDetection> detections;

public:

  // default constructor
  Demo() :
    // default settings, most can be modified through command line options (see below)
    m_tagDetector(NULL),
    m_tagCodes(AprilTags::tagCodes36h11),

    m_draw(true),
    m_arduino(false),
    m_timing(false),

    m_width(640),
    m_height(480),
    m_tagSize(0.166),
    m_fx(800),
    m_fy(800),
    m_px(m_width/2),
    m_py(m_height/2),

    m_exposure(-1),
    m_gain(-1),
    m_brightness(-1),

    m_deviceId(DEFAULT_CAMERA),

    m_ros(true)

  {}

  // changing the tag family
  void setTagCodes(string s) {
    if (s=="16h5") {
      m_tagCodes = AprilTags::tagCodes16h5;
    } else if (s=="25h7") {
      m_tagCodes = AprilTags::tagCodes25h7;
    } else if (s=="25h9") {
      m_tagCodes = AprilTags::tagCodes25h9;
    } else if (s=="36h9") {
      m_tagCodes = AprilTags::tagCodes36h9;
    } else if (s=="36h11") {
      m_tagCodes = AprilTags::tagCodes36h11;
    } else {
      cout << "Invalid tag family specified" << endl;
      exit(1);
    }
  }

  // parse command line options to change default behavior
  void parseOptions(int argc, char* argv[]) {
    int c;
    while ((c = getopt(argc, argv, ":h?adtrC:F:H:S:W:E:G:B:D:")) != -1) {
      // Each option character has to be in the string in getopt();
      // the first colon changes the error character from '?' to ':';
      // a colon after an option means that there is an extra
      // parameter to this option; 'W' is a reserved character
      switch (c) {
      case 'h':
      case '?':
        cout << intro;
        cout << usage;
        exit(0);
        break;
      case 'a':
        m_arduino = true;
        break;
      case 'd':
        m_draw = false;
        break;
      case 't':
        m_timing = true;
        break;
      case 'r':
        m_ros = false;
        break;
      case 'C':
        setTagCodes(optarg);
        break;
      case 'F':
        m_fx = atof(optarg);
        m_fy = m_fx;
        break;
      case 'H':
        m_height = atoi(optarg);
        m_py = m_height/2;
         break;
      case 'S':
        m_tagSize = atof(optarg);
        break;
      case 'W':
        m_width = atoi(optarg);
        m_px = m_width/2;
        break;
      case 'E':
#ifndef EXPOSURE_CONTROL
        cout << "Error: Exposure option (-E) not available" << endl;
        exit(1);
#endif
        m_exposure = atoi(optarg);
        break;
      case 'G':
#ifndef EXPOSURE_CONTROL
        cout << "Error: Gain option (-G) not available" << endl;
        exit(1);
#endif
        m_gain = atoi(optarg);
        break;
      case 'B':
#ifndef EXPOSURE_CONTROL
        cout << "Error: Brightness option (-B) not available" << endl;
        exit(1);
#endif
        m_brightness = atoi(optarg);
        break;
      case 'D':
        m_deviceId = atoi(optarg);
        break;
      case ':': // unknown option, from getopt
        cout << intro;
        cout << usage;
        exit(1);
        break;
      }
    }

    if (argc > optind) {
      for (int i=0; i<argc-optind; i++) {
        m_imgNames.push_back(argv[optind+i]);
      }
    }
  }

  void setup() {
    m_tagDetector = new AprilTags::TagDetector(m_tagCodes);

    if(m_ros) {
      tag_pose_pub = nh.advertise<geometry_msgs::PoseStamped>
              ("apriltag/local", 10);
    }

    // prepare window for drawing the camera images
    if (m_draw) {
      cv::namedWindow(windowName, 1);
    }

    // optional: prepare serial port for communication with Arduino
    if (m_arduino) {
      m_serial.open("/dev/ttyACM0");
    }
  }

  void setupVideo() {

#ifdef EXPOSURE_CONTROL
    // manually setting camera exposure settings; OpenCV/v4l1 doesn't
    // support exposure control; so here we manually use v4l2 before
    // opening the device via OpenCV; confirmed to work with Logitech
    // C270; try exposure=20, gain=100, brightness=150

    string video_str = "/dev/video1";
    video_str[10] = '0' + m_deviceId;
    int device = v4l2_open(video_str.c_str(), O_RDWR | O_NONBLOCK);

    if (m_exposure >= 0) {
      // not sure why, but v4l2_set_control() does not work for
      // V4L2_CID_EXPOSURE_AUTO...
      struct v4l2_control c;
      c.id = V4L2_CID_EXPOSURE_AUTO;
      c.value = 1; // 1=manual, 3=auto; V4L2_EXPOSURE_AUTO fails...
      if (v4l2_ioctl(device, VIDIOC_S_CTRL, &c) != 0) {
        cout << "Failed to set... " << strerror(errno) << endl;
      }
      cout << "exposure: " << m_exposure << endl;
      v4l2_set_control(device, V4L2_CID_EXPOSURE_ABSOLUTE, m_exposure*6);
    }
    if (m_gain >= 0) {
      cout << "gain: " << m_gain << endl;
      v4l2_set_control(device, V4L2_CID_GAIN, m_gain*256);
    }
    if (m_brightness >= 0) {
      cout << "brightness: " << m_brightness << endl;
      v4l2_set_control(device, V4L2_CID_BRIGHTNESS, m_brightness*256);
    }
    v4l2_close(device);
#endif 

    // find and open a USB camera (built in laptop camera, web cam etc)
    m_cap = cv::VideoCapture(m_deviceId);
        if(!m_cap.isOpened()) {
      cerr << "ERROR: Can't find video device " << m_deviceId << "\n";
      exit(1);
    }
    m_cap.set(CV_CAP_PROP_FRAME_WIDTH, m_width);
    m_cap.set(CV_CAP_PROP_FRAME_HEIGHT, m_height);
    cout << "Camera successfully opened (ignore error messages above...)" << endl;
    cout << "Actual resolution: "
         << m_cap.get(CV_CAP_PROP_FRAME_WIDTH) << "x"
         << m_cap.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;
    // m_cap.open('1'-'0');
    #ifdef __unix__
       signal(SIGINT,quit_signal_handler); // listen for ctrl-C
    #endif
  }
  // Finds the intersection of two lines, or returns false.
  // The lines are defined by (o1, p1) and (o2, p2).
  bool intersection(Point2f o1, Point2f p1, Point2f o2, Point2f p2,
                        Point2f &r)
  {
      Point2f x = o2 - o1;
      Point2f d1 = p1 - o1;
      Point2f d2 = p2 - o2;

      float cross = d1.x*d2.y - d1.y*d2.x;
      if (abs(cross) < /*EPS*/1e-8)
          return false;

      double t1 = (x.x * d2.y - x.y * d2.x)/cross;
      r = o1 + d1 * t1;
      return true;
  }

  void print_detection(AprilTags::TagDetection& detection) const {
    // cout << "  Id: " << detection.id
    //      << "  Code: " << detection.code
    //     << " (Hamming: " << detection.hammingDistance << ")";

    // recovering the relative pose of a tag:

    // NOTE: for this to be accurate, it is necessary to use the
    // actual camera parameters here as well as the actual tag size
    // (m_fx, m_fy, m_px, m_py, m_tagSize)
    float m_tagSize;

    Eigen::Vector3d translation;
    Eigen::Matrix3d rotation;
    if(detection.id == BIG_TAG_ID)
      m_tagSize = BIG_TAG_SIZE;
    else if(detection.id == SMALL_TAG_ID)
      m_tagSize = SMALL_TAG_SIZE;
      
    detection.getRelativeTranslationRotation(m_tagSize, m_fx, m_fy, m_px, m_py,
                                                translation, rotation);

    double yaw, pitch, roll;


    Eigen::Matrix3d F;
    F <<
      1, 0,  0,
      0,  -1,  0,
      0,  0,  1;
    Eigen::Matrix3d fixed_rot = F*rotation;
    Eigen::Matrix3d localRotation;
    double rotAngle = -PI/4;
    localRotation <<
      cos(rotAngle),  -sin(rotAngle), 0,
      sin(rotAngle),  cos(rotAngle), 0,
      0, 0, 1;

    if(detection.id == SMALL_TAG_ID)
      fixed_rot = fixed_rot*localRotation;

    wRo_to_euler(fixed_rot, yaw, pitch, roll);
    

    cout << std::fixed
         << "  distance=" << std::setprecision(5) << translation.norm()
         << "m, x=" << std::setprecision(5) << translation(0)
         << ", y=" << std::setprecision(5) << translation(1)
         << ", z=" << std::setprecision(5) << translation(2)
         << ", yaw=" << std::setprecision(5) << yaw
         << ", pitch=" << std::setprecision(5) << pitch
         << ", roll=" << std::setprecision(5) << roll
         << endl;

    // Send ROS message
    if(m_ros) {
      geometry_msgs::PoseStamped pose;
      pose.pose.position.x = translation(0);
      pose.pose.position.y = translation(1);
      pose.pose.position.z = translation(2);
      tag_pose_pub.publish(pose);
      ros::spinOnce();
    }

    // Also note that for SLAM/multi-view application it is better to
    // use reprojection error of corner points, because the noise in
    // this relative pose is very non-Gaussian; see iSAM source code
    // for suitable factors.
  }

  void processImage(cv::Mat& image, cv::Mat& image_gray, cv::Mat& prevGray, TermCriteria termcrit, Size subPixWinSize, Size winSize, bool* needToInit, bool* addRemovePt) {
    // alternative way is to grab, then retrieve; allows for
    // multiple grab when processing below frame rate - v4l keeps a
    // number of frames buffered, which can lead to significant lag
    //      m_cap.grab();
    //      m_cap.retrieve(image);

    // detect April tags (requires a gray scale image)
    cv::cvtColor(image, image_gray, CV_BGR2GRAY);

    // show the current image including any detections
    
    //printf("Need to Init %d\n", *needToInit);
    if( *needToInit || points[0].empty() || points[0].size()<4)
    {
        // printf("Searching for April Tags..\n");
        double t0;
        if (m_timing) {
          t0 = tic();
        }
        detections.clear();
        detections = m_tagDetector->extractTags(image_gray);
        if (m_timing) {
          double dt = tic()-t0;
          cout << "Extracting tags took " << dt << " seconds." << endl;
        }

        points[1].clear();
        if(!detections.empty()) {
          int index = 0;
          for(int i=0; i<detections.size(); i++) {
            if(detections[i].id == BIG_TAG_ID || detections[i].id == SMALL_TAG_ID) {
              index = i;
            }
          }
          AprilTags::TagDetection detect = detections[index];
          detections.clear();
          detections.push_back(detect);
          //printf("Detected %f %f\n", (float)detect.cxy.first, (float)detect.cxy.second);
          //Point2f centre = Point2f((float)detect.cxy.first, (float)detect.cxy.second);
          Point2f p1 = Point2f((float)detect.p[0].first, (float)detect.p[0].second);
          Point2f p2 = Point2f((float)detect.p[1].first, (float)detect.p[1].second);
          Point2f p3 = Point2f((float)detect.p[2].first, (float)detect.p[2].second);
          Point2f p4 = Point2f((float)detect.p[3].first, (float)detect.p[3].second);
          //points[1].push_back(centre);
          points[1].push_back(p1);
          points[1].push_back(p2);
          points[1].push_back(p3);
          points[1].push_back(p4);
        }
        // automatic initialization
        //goodFeaturesToTrack(image_gray, points[1], MAX_COUNT, 0.01, 10, Mat(), 3, 0, 0.04);
        //cornerSubPix(image_gray, points[1], subPixWinSize, Size(-1,-1), termcrit);
        *addRemovePt = false;
        //print out each detection
        for (int i=0; i<detections.size(); i++) {
          print_detection(detections[i]);
        }
    }
    else if( !points[0].empty() )
    {
        vector<uchar> status;
        vector<float> err;
        if(prevGray.empty())
            image_gray.copyTo(prevGray);
        calcOpticalFlowPyrLK(prevGray, image_gray, points[0], points[1], status, err, winSize,
                             3, termcrit, 0, 0.001);
        size_t i, k;
        for( i = k = 0; i < points[1].size(); i++ )
        {
            if( *addRemovePt )
            {
                if( norm(point - points[1][i]) <= 5 )
                {
                    *addRemovePt = false;
                    continue;
                }
            }

            if( !status[i] )
                continue;
            points[1][k++] = points[1][i];
            circle( image, points[1][i], 3, Scalar(0,255,0), -1, 8);
        }
        points[1].resize(k);
    }
    if(points[1].size() % 4 == 0) {
      for(int i=0; i<points[1].size()/4; i++) {
        Point2f intersec;
        intersection(points[1][i+0], points[1][i+2], points[1][i+1], points[1][i+3], intersec);
        circle( image, intersec, 3, Scalar(255,0,0), -1, 8);

        //Create detection from LK result
        AprilTags::TagDetection detectionLK = detections[i];
        cout << "Tag: " << detections[i].id;
        detectionLK.p[0] = std::make_pair(points[1][i+0].x, points[1][i+0].y);
        detectionLK.p[1] = std::make_pair(points[1][i+1].x, points[1][i+1].y);
        detectionLK.p[2] = std::make_pair(points[1][i+2].x, points[1][i+2].y);
        detectionLK.p[3] = std::make_pair(points[1][i+3].x, points[1][i+3].y);
        //print out each detection
        print_detection(detectionLK);

      }
    }
  
    if( *addRemovePt && points[1].size() < (size_t)MAX_COUNT )
    {
        vector<Point2f> tmp;
        tmp.push_back(point);
        cornerSubPix( image_gray, tmp, winSize, cvSize(-1,-1), termcrit);
        points[1].push_back(tmp[0]);
        *addRemovePt = false;
    }

    *needToInit = false;

    if (m_draw) {
      imshow("LK Demo", image);
      for (int i=0; i<detections.size(); i++) {
        // also highlight in the image
        detections[i].draw(image);
      }
      imshow(windowName, image); // OpenCV call
    }

    // optionally send tag information to serial port (e.g. to Arduino)
    if (m_arduino) {
      if (detections.size() > 0) {
        // only the first detected tag is sent out for now
        Eigen::Vector3d translation;
        Eigen::Matrix3d rotation;
        detections[0].getRelativeTranslationRotation(m_tagSize, m_fx, m_fy, m_px, m_py,
                                                     translation, rotation);
        m_serial.print(detections[0].id);
        m_serial.print(",");
        m_serial.print(translation(0));
        m_serial.print(",");
        m_serial.print(translation(1));
        m_serial.print(",");
        m_serial.print(translation(2));
        m_serial.print("\n");
      } else {
        // no tag detected: tag ID = -1
        m_serial.print("-1,0.0,0.0,0.0\n");
      }
    }
  }

  // Load and process a single image
  void loadImages() {
    cv::Mat image;
    cv::Mat image_gray;
    TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 20, 0.03);
    Size subPixWinSize(10,10), winSize(31,31);
    bool needToInit=false;
    bool addRemovePt=false;
    for (list<string>::iterator it=m_imgNames.begin(); it!=m_imgNames.end(); it++) {
      image = cv::imread(*it); // load image with opencv
      processImage(image, image_gray, image_gray, termcrit, subPixWinSize, winSize, &needToInit, &addRemovePt);
      while (cv::waitKey(100) == -1) {}
    }
  }

  // Video or image processing?
  bool isVideo() {
    return m_imgNames.empty();
  }

  // The processing loop where images are retrieved, tags detected,
  // and information about detections generated
  void loop() {

    cv::Mat image;
    cv::Mat image_gray;
    cv::Mat prevGray;

    TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 20, 0.03);
    Size subPixWinSize(10,10), winSize(31,31);

    int frame = 0;
    bool needToInit=false;
    bool addRemovePt=false;
    double last_t = tic();
    while (true) {

      // capture frame
      m_cap >> image;
      // m_cap.read(image);
      if(image.empty())printf("Empty Frame\n");
      if (quit_signal) exit(0); // exit cleanly on interrupt
      needToInit = (frame%RESET_FRAME_NO == 0);

      processImage(image, image_gray, prevGray, termcrit, subPixWinSize, winSize, &needToInit, &addRemovePt);

      // print out the frame rate at which image frames are being processed
      frame++;
      if (frame % 10 == 0) {
        double t = tic();
        cout << "  " << 10./(t-last_t) << " fps" << endl;
        last_t = t;
      }

      // exit if any key is pressed
      char c = (char)waitKey(10);
      if( c == 27 )
          break;
      switch( c )
      {
      case 'r':
          needToInit = true;
          break;
      case 'c':
          points[0].clear();
          points[1].clear();
          break;
      }

      std::swap(points[1], points[0]);
      cv::swap(prevGray, image_gray);
    }
  }

}; // Demo


// here is were everything begins
int main(int argc, char* argv[]) {
  ros::init(argc, argv, "marker_detector");
  Demo demo;
  // process command line options
  demo.parseOptions(argc, argv);

  demo.setup();

  if (demo.isVideo()) {
    cout << "Processing video" << endl;

    // setup image source, window for drawing, serial port...
    demo.setupVideo();

    // the actual processing loop where tags are detected and visualized
    demo.loop();

  } else {
    cout << "Processing image" << endl;

    // process single image
    demo.loadImages();

  }

  return 0;
}
